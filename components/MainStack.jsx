import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ParkScreen from '../screens/ParkScreen';
import QrScreen from '../screens/QrScreen';

const Stack = createStackNavigator();

export default function MainStack({route}) {
    const backgroundColor = '#011826';
    const { uId } = route.params;
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ParkScreen"
        component={ParkScreen}
        initialParams={{ uId: uId}}
        options={{
          title: 'Home',
          headerStyle: { backgroundColor: backgroundColor },
          headerTintColor: 'white',
          headerBackTitle: 'Volver',
          headerBackTitleVisible: false,
        }}
      />
      
      <Stack.Screen name="QrScreen" component={QrScreen} options={{  title: 'Código QR',
          headerStyle: { backgroundColor: backgroundColor },
          headerTintColor: 'white',
          headerBackTitle: 'Volver',
          headerBackTitleVisible: false, }} />
    </Stack.Navigator>
  );
    };
