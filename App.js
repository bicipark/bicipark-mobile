import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './screens/LoginScreen';
import MainStack from './components/MainStack';
import RegistroScreen from './screens/RegisterScreen';
import { Ionicons } from '@expo/vector-icons';



const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const backgroundColor = '#011826';

function HomeTabs({ route }) {
  return (
    <Tab.Navigator
      screenOptions={
        {
          tabBarLabelStyle: {color: 'white'},
          tabBarStyle: {backgroundColor: backgroundColor},
          tabBarInactiveTintColor: 'white', // Color de iconos cuando no están seleccionados
          tabBarActiveTintColor: '#F29863', // Color de iconos cuando están seleccionados
        }
      }
    >
      <Tab.Screen
        name="MainStack"
        component={MainStack}
        initialParams={{ uId: route.params.uId }}
        options={{
          title: 'Home',
          headerShown: false, // Oculta el header en la pantalla de Home
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="home" size={size} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
       <StatusBar translucent={true} style="light" />
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{
            title: 'Login',
            headerShown: false, // Oculta el header en la pantalla de Home
          }}
        />
        <Stack.Screen
          name="Home"
          component={HomeTabs}
          //initialParams={{ uId: 'valor_predeterminado' }}
          options={{
            title: 'Home',
            headerShown: false, // Oculta el header en la pantalla de Home
          }}
        />
        <Stack.Screen
        name="Registro"
        component={RegistroScreen}
        options={{
          title: 'Registro',
          headerStyle: { backgroundColor: backgroundColor },
          headerTintColor: 'white',
          headerBackTitle: 'Volver'
        }}
      />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
