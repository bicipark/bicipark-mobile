import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  KeyboardAvoidingView,
  Alert,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import { API_URL } from '../utils/constants';

export default function RegisterScreen({ navigation }) {
  const [name, setName] = useState('');
  const [rut, setRut] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isValidEmail, setIsValidEmail] = useState(true);

  useEffect(() => {
    return () => {
      setIsLoading(false);
    };
  }, []);

  const handleRegistro = async () => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(email)) {
      setIsValidEmail(false);
      return;
    }

    const registroData = {
      name,
      rut,
      email,
      password,
    };

    try {
      setIsLoading(true);

      const response = await axios.post(`${API_URL}/auth/register`, registroData, {
        headers: {
          'Content-Type': 'application/json',
        },
      });

      setIsLoading(false);

      if (response.status === 201) {
        Alert.alert(
          'Registro Exitoso',
          '¡El usuario ha sido registrado exitosamente!',
          [
            {
              text: 'OK',
              onPress: () => navigation.goBack(),
            },
          ],
          { cancelable: false }
        );
      } else {
        Alert.alert(
          'Error de Registro',
          'Hubo un problema al intentar registrar el usuario. Por favor, inténtalo de nuevo.',
          [
            {
              text: 'OK',
              onPress: () => console.log('OK Pressed'),
            },
          ],
          { cancelable: false }
        );
      }
    } catch (error) {
      setIsLoading(false);
      console.error(error);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <KeyboardAvoidingView
        style={[styles.container, { backgroundColor: '#011826' }]}
        behavior="padding"
      >
        <Image source={require('../assets/logo.png')} style={styles.logo} />
        <Text style={[styles.title, { color: 'white' }]}>Registro</Text>
        <TextInput
          style={[styles.input, styles.roundedInput, { color: 'white' }]}
          placeholder="Nombre Completo"
          onChangeText={(text) => setName(text)}
          placeholderTextColor="#D0D5DD"
          value={name}
        />

        <TextInput
          style={[styles.input, styles.roundedInput, { color: 'white' }]}
          placeholder="Rut"
          onChangeText={(text) => setRut(text)}
          placeholderTextColor="#D0D5DD"
          value={rut}
        />

        <TextInput
          style={[styles.input, styles.roundedInput, { color: 'white' }]}
          placeholder="Correo electrónico"
          onChangeText={(text) => {
            setEmail(text);
            setIsValidEmail(true);
          }}
          placeholderTextColor="#D0D5DD"
          value={email}
        />

        {!isValidEmail && (
          <Text style={{ color: 'red' }}>Ingrese un correo electrónico válido.</Text>
        )}

        <TextInput
          style={[styles.input, styles.roundedInput, { color: 'white' }]}
          placeholder="Contraseña"
          onChangeText={(text) => setPassword(text)}
          placeholderTextColor="#D0D5DD"
          value={password}
          secureTextEntry
        />

        <TouchableOpacity
          style={[styles.button, { backgroundColor: '#F27649' }]}
          onPress={handleRegistro}
        >
          <Text style={styles.buttonText}>Registrarse</Text>
        </TouchableOpacity>

        {isLoading && (
          <ActivityIndicator size="large" color="#F29863" style={{ marginTop: 20 }} />
        )}
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    width: '100%',
    height: 40,
    borderColor: '#7B95A6',
    borderWidth: 1,
    marginBottom: 10,
    paddingLeft: 10,
  },
  roundedInput: {
    borderRadius: 8,
  },
  button: {
    backgroundColor: '#F27649',
    borderRadius: 32,
    padding: 10,
    width: '100%',
    alignItems: 'center',
    marginTop: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  logo: {
    width: 200, // Ajusta el ancho de acuerdo a tus necesidades
    height: 200, // Ajusta el alto de acuerdo a tus necesidades
    marginBottom: 20,
  },
});
