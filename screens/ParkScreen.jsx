import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity, StyleSheet, ActivityIndicator, Text, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { API_URL } from '../utils/constants';
import axios from 'axios';
import RNPickerSelect from 'react-native-picker-select';

export default function ParkScreen({ route }) {
  const { uId } = route.params;
  const [isLoading, setIsLoading] = useState(false);
  const [parkingData, setParkingData] = useState([]);
  const [selectedPark, setSelectedPark] = useState('');

  const navigation = useNavigation();

  const fetchParkingStatus = async () => {
    try {
      const response = await axios.get(`${API_URL}/park/getStatus`);
      setParkingData(response.data);
    } catch (error) {
      console.error('Error fetching parking status:', error);
    }
  };

  const handleParkRequest = async () => {
    try {
      setIsLoading(true);
      console.log('Requesting park:', selectedPark);
      const response = await axios.post(`${API_URL}/park`, {
        numPark: parseInt(selectedPark),
        userId: uId,
      });

      fetchParkingStatus();

      if (response.data.qrCodeUrl) {
        navigation.navigate('QrScreen', { qrCodeUrl: response.data.qrCodeUrl, selectedPark: selectedPark });
      }
    } catch (error) {
      console.error('Error requesting park:', error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchParkingStatus();
  }, []);

  const availableParkingCount = parkingData.filter((park) => !park.inUse).length;

  return (
    <View style={[styles.container, { backgroundColor: '#011826' }]}>
      <View style={styles.imageContainer}>
        <Image style={styles.topImage} source={require('../assets/image.png')} />
      </View>
      <View style={styles.contentContainer}>
        <View style={styles.availableParkingContainer}>
          <Text style={styles.availableParkingText}>
            Estacionamientos disponibles: {availableParkingCount}
          </Text>
        </View>
        <View style={styles.pickerAndButtonContainer}>
          <Text style={styles.text}>Seleccione el número de estacionamiento:</Text>
          <RNPickerSelect
            placeholder={{ label: 'Selecciona un estacionamiento', value: null }}
            items={parkingData
              .filter((park) => !park.inUse)
              .map((park) => ({
                label: `Estacionamiento ${park.numPark}`,
                value: park.numPark.toString(),
              }))}
            onValueChange={(value) => setSelectedPark(value)}
            style={{
              ...pickerSelectStyles,
              iconContainer: {
                top: 10,
                right: 12,
              },
              inputIOS: {
                ...pickerSelectStyles.inputIOS,
                borderColor: '#F27649',
                borderRadius: 8,
              },
              inputAndroid: {
                ...pickerSelectStyles.inputAndroid,
                borderColor: '#F27649',
                borderRadius: 8,
              },
            }}
            useNativeAndroidPickerStyle={false}
          />
          <TouchableOpacity
            style={[styles.button, { backgroundColor: '#F27649' }]}
            onPress={handleParkRequest}
            disabled={!selectedPark}
          >
            <Text style={styles.buttonText}>Solicitar estacionamiento</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'white',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'white',
    paddingRight: 30,
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0,
  },
  availableParkingContainer: {
    marginBottom: 10,
    marginTop: 10,
  },
  availableParkingText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  button: {
    borderRadius: 32,
    padding: 10,
    width: '100%',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: -10,
    backgroundColor: '#F27649',
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  text: {
    color: 'white',
    marginBottom: 10,
  },
  imageContainer: {
    height: '60%',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: -40,
    borderColor: '#F27649',
    borderWidth: 0,
    borderBottomWidth: 2,
  },
  topImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  contentContainer: {
    flex: 2 / 3,
    justifyContent: 'space-between',
  },
  pickerAndButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
