import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const QrScreen = ({ route }) => {
  const { qrCodeUrl, selectedPark } = route.params;
  const [qrCode, setQrCode] = useState('');

  useEffect(() => {
    setQrCode(qrCodeUrl);
  }, [qrCodeUrl]);

  return (
    <View style={styles.container}>
        
      <Text style={styles.headerText}>¡Tu bicicleta está estacionada!</Text>
      <Text style={styles.headerText}>Estacionamiento Nro. {selectedPark}</Text>
      {qrCode && (
        <View style={styles.qrCodeContainer}>
          <Image style={styles.qrCodeImage} source={{ uri: qrCode }} />
        </View>
      )}
      <Text style={styles.instructionsText}>Escanea este código en la salida.</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#011826',
  },
  headerText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 20,
  },
  qrCodeContainer: {
    borderWidth: 8,
    borderColor: '#F27649',
    borderRadius: 10,
    padding: 5,
    margin: 10,
  },
  qrCodeImage: {
    width: 300,
    height: 300,
    resizeMode: 'contain',
  },
  instructionsText: {
    fontSize: 16,
    color: 'white',
    marginTop: 20,
  },
});

export default QrScreen;
