import { useNavigation } from '@react-navigation/native';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TextInput,
    Button,
    TouchableOpacity,
    StyleSheet,
    TouchableWithoutFeedback,
    Keyboard,
    Image,
    KeyboardAvoidingView,
    ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import { API_URL } from '../utils/constants';

export default function LoginScreen() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const hardcodedCredentials = {
    email: 'user',
    password: '123',
  };
  

  useEffect(() => {
    return () => {
      // Cleanup function to reset isLoading when the component unmounts
      setIsLoading(false);
    };
  }, []);

  const handleLogin = async () => {
    const loginData = {
      email,
      password,
    };

    try {
      const response = await axios.post(`${API_URL}/auth/login`, loginData, {
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        navigation.navigate('Home', { uId: response.data.id });
      }
    } catch (error) {
      alert('Credenciales incorrectas');
    }
  };

  const handleRegistro = () => {
    navigation.navigate('Registro');
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <KeyboardAvoidingView
        style={[styles.container, { backgroundColor: '#011826' }]}
        behavior="padding"
        animated={false} // Desactiva la animación
      >
        <Image source={require('../assets/logo.png')} style={styles.logo} />
        <Text style={[styles.title, { color: 'white' }]}>Inicio de sesión</Text>
        <TextInput
          style={[styles.input, styles.roundedInput, { color: 'white' }]}
          placeholder="Correo electrónico"
          placeholderTextColor="#D0D5DD"
          value={email}
          onChangeText={(text) => setEmail(text)}
        />
        <TextInput
          style={[styles.input, styles.roundedInput, { color: 'white' }]}
          placeholder="Contraseña"
          placeholderTextColor="#D0D5DD"
          secureTextEntry={true}
          value={password}
          onChangeText={(text) => setPassword(text)}
        />
        <TouchableOpacity
          style={[styles.button, { backgroundColor: '#F27649' }]}
          onPress={handleLogin}
        >
          <Text style={styles.buttonText}>Iniciar sesión</Text>
        </TouchableOpacity>
        {isLoading && (
          <ActivityIndicator size="large" color="#F29863" style={{ marginTop: 20 }} />
        )}
                   <TouchableOpacity
              style={{ backgroundColor: 'transparent',marginTop: 20, marginBottom: 20,  }}
              onPress={handleRegistro}
            >
              <Text style = {{color:'#356887',  fontWeight: 'bold', textDecorationLine: 'underline', fontSize: 16}}>¿Aún no tienes cuenta? Regístrate aquí</Text>
            </TouchableOpacity>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#7B95A6',
    marginBottom: 10,
    paddingLeft: 10,
  },
  roundedInput: {
    borderRadius: 8,
  },
  button: {
    backgroundColor: '#F27649',
    borderRadius: 32,
    padding: 10,
    width: '100%',
    alignItems: 'center',
    marginTop: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  logo: {
    width: 200,
    height: 200,
    marginBottom: 20,
  },
});